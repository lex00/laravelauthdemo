<?php


class AuthController extends BaseController {

	/**
	* Instantiate a new AuthController instance.
	*/
	public function __construct()
	{
		// csrf protection
		$this->beforeFilter('csrf', array('on' => 'post'));

	
		$this->beforeFilter(function()
		{
			//die(Route::currentRouteAction());
			if ( Auth::check() )
			{
				// logged in
				switch (Route::currentRouteAction()) {
					case 'AuthController@getSignup': 
					case 'AuthController@postSignup':
					case 'AuthController@getSignin':
					case 'AuthController@postSignin':
					case 'AuthController@getPwreminder':
					case 'AuthController@postPwreminder':
					case 'AuthController@getPwreset':
					case 'AuthController@postPwreset':
						return Redirect::to('/');
				}
			} else {
				// guest
				switch (Route::currentRouteAction()) {
					case 'AuthController@getEditProfile': 
					case 'AuthController@postEditProfile':
						return Redirect::to('/');
				}
			}
		});
	}



	/**
	* Display Signup Form
	*/
	public function getSignup()
	{
		return View::make('signup');
	}



	/**
	* Validate Signup Form
	*/
	public function postSignup()
	{
		// run validator
		$rules = array(
				'name_first' => 'required|min:2',
				'name_last' => 'required|min:2',
				'gender' => 'required|in:m,f',
				'country' => 'required|min:2|max:2',
				'url_website' => 'min:5|url',
				'email' => 'required|min:5|email|unique:users,email',
				'password' => 'required|min:6|confirmed'
			);
		$validator = Validator::make(Input::all(), $rules, Lang::get('vf.validation_messages'));


		// if we failed redirect to signup
		if ($validator->fails())
		{
			//$this->flashBootstrapClasses($validator);
			return Redirect::to('auth/signup')
				->withErrors($validator)->withInput();
		} else {
			// we passed so save the new user - active defaults true
			$user = new User;
			$user->setFirstName(Input::get('name_first'));
			$user->setLastName(Input::get('name_last'));
			$user->setGender(Input::get('gender'));
			$user->setCountry(Input::get('country'));
			$user->setWebsiteUrl(Input::get('url_website'));
			$user->setReminderEmail(Input::get('email'));
			$user->setAuthPassword(Hash::make(Input::get('password')));
			$user->save();

			//login newly saved user
			Auth::login($user);

			//redirect to confirmation screen
			return View::make('confirmed');
		}
	}



	/**
	* Display Sign in Form
	*/
	public function getSignin()
	{
		return View::make('signin');
	}



	/**
	* Validate Sign in Form
	*/
	public function postSignin()
	{
		// run validator
		$rules = array(
				'email' => 'required|min:5|email',
				'password' => 'required|min:6'
			);
		$validator = Validator::make(Input::all(), $rules, Lang::get('vf.validation_messages'));

		// if we failed redirect to sign in
		if ($validator->fails())
		{
			return Redirect::to('auth/signin')->withErrors($validator)->withInput();
		} else {
			//check for remember me
			$rememberme = false;
			if ( Input::has('rememberme') )
			{
				$rememberme = true;
			}

			//input was ok so attempt authorization
			if ( Auth::attempt( array(
					'email' => Input::get('email'), 
					'password' => Input::get('password'),
					'active' => 1
					), $rememberme ) 
				)
			{
				return Redirect::to('/');
			} else {
				Input::flash();
				return View::make('signin')->with('login_errors', true);
			}
		}

		
	}



	/**
	* Logout User
	*/
	public function getSignout()
	{
		Auth::logout();
		return Redirect::to('/');
	}



	/**
	* Display Lost PW Form
	*/
	public function getPwreminder()
	{
		return View::make('pwreminder');
	}



	/**
	* Send email reminder for pw
	*/
	public function postPwreminder()
	{
		// validate email and bounce back on failure
		$rules = array(
				'email' => 'required|min:5|email'
			);
		$validator = Validator::make(Input::all(), $rules, Lang::get('vf.validation_messages'));
		if ($validator->fails())
		{
			return Redirect::to('auth/pwreminder')->withErrors($validator)->withInput();
		}


		// success - try reminder
		$credentials = array('email' => Input::get('email'));
		return Password::remind($credentials, function($message, $user)
		{
		    $message->subject(Lang::get('pwreminder_email_subject'));
		});
	}



	/**
	* Display Reset PW Form
	*/
	public function getPwreset($token=null)
	{
		if ( $token==null )
		{
			if ( Input::has('token') )
			{ 
				$token = Input::get('token');
			}
		}		


		return View::make('pwreset')->with('token', $token);
	}



	/**
	* Reset PW 
	*/
	public function postPwreset()
	{
		$rules = array(
				'email' => 'required|min:5|email',
				'password' => 'required|min:6|confirmed'
			);
		$validator = Validator::make(Input::all(), $rules, Lang::get('vf.validation_messages'));

		if ($validator->fails())
		{
			return Redirect::to('auth/pwreset')->withErrors($validator)->withInput();
		}// else {
			//now run pw reset
			//Input::flash(); need this?
			$credentials = array('email' => Input::get('email'));
			return Password::reset($credentials, function($user, $password)
			{
				$user->password = Hash::make($password);
				$user->save();

				//login newly saved user
				Auth::login($user);

				return Redirect::to('/');
			});
		//}
		
	}



	/**
	* Display Edit Profile Form
	*/
	public function getEditProfile()
	{
		//Input::flash( 'email', Auth::user()->getReminderEmail() );
		//Input::flash( 'url_website', Auth::user()->getWebsiteUrl() );
		Form::model(Auth::user());
		return View::make('editprofile');
	}



	/**
	* Validate Edit Profile Form
	*/
	public function postEditProfile()
	{
		$rules = array(
			'url_website' => 'min:5|url',
			'password' => 'required|min:6|confirmed',
			'country' => 'required|min:2|max:2'
		);

		// if they changed their email, add it to auth rules
		if ( Input::get('email') != Auth::user()->getReminderEmail() )
		{
			$rules['email'] = 'required|min:5|email|unique:users,email';
		}
		
		$validator = Validator::make(Input::all(), $rules, Lang::get('vf.validation_messages'));

		// if we failed redirect to signup
		if ($validator->fails())
		{
			return Redirect::to('auth/edit-profile')->withErrors($validator)->withInput();
		} else {
			// we passed so update and save the logged in user
			Auth::user()->setCountry(Input::get('country'));
			Auth::user()->setWebsiteUrl(Input::get('url_website'));
			Auth::user()->setReminderEmail(Input::get('email'));
			Auth::user()->setAuthPassword(Hash::make(Input::get('password')));
			Auth::user()->save();

			//redirect to confirmation screen
			Input::flash();
			$data = array('success' => '1');
			return View::make('editprofile')->with($data);
		}
	}


}
