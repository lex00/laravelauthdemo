<?php 

return array(

	'app_title' => 'VF Challenge',

	'welcome_header' => 'Welcome to VF Challenge',

	'maintenance' => 'VF Challenge is undergoing scheduled maintenance, check back soon!',

	'menu_records' => 'View Records',
	'menu_signup' => 'Sign Up',
	'menu_signin' => 'Sign In',
	'menu_signout' => 'Sign Out',
	'menu_editprofile' => 'Edit Profile',
	'menu_addrecord' => 'Add New Record',

	'signup_header' => 'Sign Up for VF Challenge',
	'signup_name_first' => 'First Name',
	'signup_name_last' => 'Last Name',
	'signup_gender' => 'Sex',
	'signup_gender_m' => 'Male',
	'signup_gender_f' => 'Female',
	'signup_url_website' => 'Web Link',
	'signup_url_website_default' => 'http://twitter.com/yourname',
	'signup_country' => 'Country',
	'signup_pick_country' => 'Pick a country',
	'signup_email' => 'E-Mail',
	'signup_email_default' => 'yourname@gmail.com',
	'signup_password' => 'Password',
	'signup_submit' => 'SIGN UP',
	'signup_thanks1' => 'Thanks for signing up :name!',
	'signup_thanks2' => 'You can now enter your records.',

	'signin_header' => 'Sign In to VF Challenge',
	'signin_invalid' => 'Incorrect e-mail or password!',
	'signin_lostpw' => 'RESET PASSWORD',
	'signin_submit' => 'Sign In',
	'signin_remember' => 'Keep me signed in',

	'pwreminder_success' => 'Check your email for a password reset link.',
	'pwreminder_submit' => 'RESET PASSWORD',
	'pwreminder_email_subject' => 'VF Challenge Password Reset',
	'pwreminder_email_body' => 'To reset your password, click the link below',

	'pwreset_confirm' => 'Password (confirm)',
	'pwreset_submit' => 'RESET PASSWORD',

	'editprofile_header' => 'Edit Profile',
	'editprofile_success' => 'Your profile has been updated.',
	'editprofile_submit' => 'EDIT PROFILE',

	'validation_messages' => array(
		'required' => 'This field is required.',
		'min' => 'This field must be at least :min characters.',
		'unique' => 'Another account with this :attribute already exists.',
		'same' => 'Both :attribute fields must match.',
		'url' => 'Your link should be formatted like this: http://somewhere.com'
	),

);
