<?php

return array(

	'pretend' => true,
	'driver' => 'mail',
	'port' => 25,
	'host' => 'localhost',
	'from' => array('address' => 'admin@local.vfchallenge.com', 'name' => 'VF Challenge'),

);
