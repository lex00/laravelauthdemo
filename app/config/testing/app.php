<?php

return array(

	'debug' => true,
	'url' => 'http://local.vfchallenge.com',

	'tests' => array(
		'name_first' => 'Jon',
		'name_last' => 'Doe',
		'url_website' => 'http://doe.com/jon',
		'country' => 'US',
		'gender' => 'm',
		'email' => 'jon@doe.com',
		'password' => 'jondoesecret123',
		'token' => 'aaaaaaaaaabbbbbbbbbbccccccccccdd',
		'active' => '1',
		),
);
