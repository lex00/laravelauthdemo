<?php

class UserTest extends TestCase {

	/**
	 * test creation and deleting of new user object
	 *
	 * @return void
	 */
	public function testCreateDeleteUser()
	{
		//set email and save a new user
		$newemail = Config::get('app.tests.email').'x';
		$user = new User;
		$user->setFirstName(Config::get('app.tests.name_first'));
		$user->setLastName(Config::get('app.tests.name_last'));
		$user->setGender(Config::get('app.tests.gender'));
		$user->setCountry(Config::get('app.tests.country'));
		$user->setWebsiteUrl(Config::get('app.tests.url_website'));
		$user->setReminderEmail($newemail);
		$user->setAuthPassword(Config::get('app.tests.password'));
		$user->save();

		//lookup again by id and verify props
		$userid = $user->getAuthIdentifier();
		$newuser = User::find($userid);
		$this->assertEquals( $newuser->getFirstName(), Config::get('app.tests.name_first') );
		$this->assertEquals( $newuser->getLastName(), Config::get('app.tests.name_last') );
		$this->assertEquals( $newuser->getGender(), Config::get('app.tests.gender') );
		$this->assertEquals( $newuser->getCountry(), Config::get('app.tests.country') );
		$this->assertEquals( $newuser->getWebsiteUrl(), Config::get('app.tests.url_website') );
		$this->assertEquals( $newuser->getReminderEmail(), $newemail );
		$this->assertEquals( $newuser->getAuthPassword(), Config::get('app.tests.password') );

		//delete user
		$user->delete();
		$newuser = User::find($userid);
		$this->assertNull($newuser);

		//make sure it is really gone from the db
		$users = User::where('email', '=', $newemail)->get();
		$this->assertEquals( count($users), 0 );
	}



	/**
	 * test read of user object from seeding
	 *
	 * @return void
	 */
	public function testReadUser()
	{
		$users = User::where('email', '=', Config::get('app.tests.email'))->get();
		$this->assertEquals( count($users), 1 );
		$user = $users[0];
		$this->assertEquals( $user->getReminderEmail(), Config::get('app.tests.email') );
	}



	/**
	 * test update of user object from seeding
	 *
	 * @return void
	 */
	public function testUpdateUser()
	{
		//lookup by email
		$users = User::where('email', '=', Config::get('app.tests.email'))->get();
		$this->assertEquals( count($users), 1 );
		$user = $users[0];
		
		//update first name
		$newname = Config::get('app.tests.name_first').'x';
		$user->setFirstName($newname);
		$user->save();

		//lookup by email again and verify first name
		$users = User::where('email', '=', Config::get('app.tests.email'))->get();
		$user = $users[0];
		$this->assertEquals( $user->getFirstName(), $newname );
	}

}
