<?php

class AuthTest extends TestCase {

	/**
	 * test loading of sign up page
	 *
	 * @return void
	 */
	public function testGetSignup()
	{
		$response = $this->call('GET', 'auth/signup');
		$this->assertResponseOk();		
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.signup_submit')));
	}


	
	/**
	 * test rejected sign up of new user
	 *
	 * @return void
	 */
	public function testRejectedUserSignup()
	{
		$response = $this->call( 'POST', 'auth/signup', array() ); // send no params for failure
		$this->assertResponseStatus(302); //should be a redirect
		$this->assertRedirectedTo('auth/signup'); //redirected back to sign up form
		$this->assertSessionHas('errors');
		$this->assertNotNull(Session::get('errors')->first('email'));
	}



	/**
	 * test successfull sign up of new user
	 *
	 * @return void
	 */
	public function testUserSignup()
	{
		//signup new user
		$newemail = Config::get('app.tests.email').'x';

		$response = $this->call( 'POST', 'auth/signup', 
						array(
						'name_first'=>Config::get('app.tests.name_first'),
						'name_last'=>Config::get('app.tests.name_last'),
						'gender'=>Config::get('app.tests.gender'),
						'country'=>Config::get('app.tests.country'),
						'url_website'=>Config::get('app.tests.url_website'),
						'email'=>$newemail,
						'password'=>Config::get('app.tests.password'),
						'password_confirmation'=>Config::get('app.tests.password')
						) 
					);
		$this->assertResponseOk();
		$content = $response->getContent();

		$this->assertTrue(str_contains($content, Lang::get('vf.signup_thanks2')));

		//lookup new user in db by email
		$users = User::where('email', '=', $newemail)->get();
		$this->assertEquals( count($users), 1 );
		$user = $users[0];
		$this->assertEquals( $user->getReminderEmail(), $newemail );
	}



	/**
	 * test loading of sign in page
	 *
	 * @return void
	 */
	public function testGetSignin()
	{
		$response = $this->call('GET', 'auth/signin');
		$this->assertResponseOk();
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.signin_submit')));
	}



	/**
	 * test rejected sign in
	 *
	 * @return void
	 */
	public function testRejectedUserSignin()
	{
		$badpw = Config::get('app.tests.password').'x';
		$response = $this->call( 'POST', 'auth/signin', array(
						'email'=> Config::get('app.tests.email'),
						'password'=> $badpw
						) 
					);
		$this->assertTrue(Auth::guest());
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.signin_invalid')));
	}



	/**
	 * test successfull sign in
	 *
	 * @return void
	 */
	public function testUserSignin()
	{
		$response = $this->call( 'POST', 'auth/signin', array(
						'email'=>Config::get('app.tests.email'),
						'password'=>Config::get('app.tests.password')
						) 
					);
		$this->assertResponseStatus(302); 
		$this->assertRedirectedTo('/'); //redirected home on success
		$this->assertTrue(Auth::check());
	}


	
	/**
	 * test loading of pw reminder page
	 *
	 * @return void
	 */
	public function testGetPwreminder()
	{
		$response = $this->call('GET', 'auth/pwreminder');
		$this->assertResponseOk();
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.pwreminder_submit')));
	}



	/**
	 * test rejected pw reminder 
	 *
	 * @return void
	 */
	public function testRejectedPwreminder()
	{
		$response = $this->call( 'POST', 'auth/pwreminder', array(
						'email'=> Config::get('app.tests.email').'junk'
						) 
					);
		$this->assertResponseStatus(302); 
		$this->assertRedirectedTo('auth/pwreminder'); //redirected back to form
		$this->assertSessionHas('error');
	}




	/**
	 * test successfull pw reminder 
	 *
	 * @return void
	 */
	public function testSendReminder()
	{
		//first send the reminder to create the token
		$response = $this->call( 'POST', 'auth/pwreminder', array(
						'email' => Config::get('app.tests.email')
						) 
					);
		$this->assertResponseStatus(302); 
		$this->assertRedirectedTo('auth/pwreminder'); //redirected back to form
		$this->assertSessionHas('success');
	}



	/**
	 * test loading of pw reset page
	 *
	 * @return void
	 */
	public function testGetPwreset()
	{
		$response = $this->call('GET', 'auth/pwreset');
		$this->assertResponseOk();
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.pwreset_submit')));
	}



	/**
	 * test failed pw reset 
	 *
	 * @return void
	 */
	public function testFailedPwReset()
	{
		//now reset pw
		$response = $this->call( 'POST', 'auth/pwreset', array(
						'email'=> Config::get('app.tests.email'),
						'password'=> Config::get('app.tests.password'),
						'password_confirmation'=> Config::get('app.tests.password'),
						'token'=>'badtoken'
						) 
					);
		$this->assertResponseStatus(302); 
		$this->assertRedirectedTo('auth/pwreset'); //redirected back to form
	}



	/**
	 * test successfull pw reset 
	 *
	 * @return void
	 */
	public function testPwReset()
	{
		$newpw = Config::get('app.tests.password').'new';

		//manually insert new token...
		DB::insert('insert into password_reminders (email, token, created_at) 
				values (?, ?, ?)', 
				array(Config::get('app.tests.email'), Config::get('app.tests.token'), date("Y-m-d H:i:s")));

		//now reset pw
		$response = $this->call( 'POST', 'auth/pwreset', array(
						'email'=>Config::get('app.tests.email'),
						'password'=>$newpw,
						'password_confirmation'=>$newpw,
						'token'=>Config::get('app.tests.token')
						) 
					);
		$this->assertResponseStatus(302); 
		$this->assertRedirectedTo('/'); //redirected back home

		//lookup user and verify new pw
		$users = User::where('email', '=', Config::get('app.tests.email'))->get();
		$this->assertEquals( count($users), 1 );
		$user = $users[0];
		$this->assertTrue( Hash::check($newpw, $user->getAuthPassword()) );
	}



	/**
	 * test failed loading of edit profile page
	 *
	 * @return void
	 */
	public function testGetEditProfileFailure()
	{
		$response = $this->call('GET', 'auth/edit-profile');
		$this->assertResponseStatus(302); 
		$this->assertRedirectedTo('/'); //redirected back home
	}



	/**
	 * test successfull loading of edit profile page
	 *
	 * @return void
	 */
	public function testGetEditProfile()
	{
		// login user
		$user = User::find(1);
		$this->be($user);

		//now load form
		$response = $this->call('GET', 'auth/edit-profile');
		$this->assertResponseOk();
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.editprofile_submit')));
	}



	/**
	 * test edit profile page
	 *
	 * @return void
	 */
	public function testPostEditProfile()
	{
		// login user
		$user = User::find(1);
		$this->be($user);
		$newfirstname = Config::get('app.tests.name_first').'x';
		$newlastname = Config::get('app.tests.name_last').'x';
		$newgender = 'z';
		$newcountry = 'ZA';
		$newsite = Config::get('app.tests.url_website').'x';
		$newemail = Config::get('app.tests.email').'x';
		$newpw = Config::get('app.tests.password').'x';

		//reset pw
		$response = $this->call( 'POST', 'auth/edit-profile', array(
						'country'=>$newcountry,
						'url_website'=>$newsite,						
						'email'=>$newemail,
						'password'=>$newpw,
						'password_confirmation'=>$newpw
						) 
					);
		$this->assertResponseOk();
		$content = $response->getContent();
		$this->assertTrue(str_contains($content, Lang::get('vf.editprofile_success')));

		//verify
		$users = User::where('email', '=', $newemail)->get();
		$this->assertEquals( count($users), 1 );
		$user = $users[0];
		$this->assertEquals( $user->getWebsiteUrl(), $newsite );
		$this->assertEquals( $user->getCountry(), $newcountry );
		$this->assertEquals( $user->getReminderEmail(), $newemail );
		$this->assertTrue( Hash::check($newpw, $user->getAuthPassword()) );
	}


}
