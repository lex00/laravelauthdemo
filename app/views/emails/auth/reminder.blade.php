<!DOCTYPE html>
<html lang="{{ Config::get('app.locale'); }}">
	<head>
		<meta charset="utf-8">
		<title>{{ Lang::get('vf.pwreminder_email_subject') }}</title>
	</head>
	<body>
		<h2>{{ Lang::get('vf.pwreminder_email_subject') }}</h2>

		<div>
			{{ Lang::get('vf.pwreminder_email_body') }}
			<br />
			{{ URL::to('auth/pwreset', array($token)) }}.
		</div>
	</body>
</html>
