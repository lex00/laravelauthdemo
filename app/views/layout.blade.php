<!DOCTYPE html>
<html lang="{{ Config::get('app.locale'); }}">
    <head>
	<title>{{ Lang::get('vf.app_title') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	{{ HTML::style('css/bootstrap.min.css') }}
	{{-- HTML::style('css/bootstrap-theme.min.css') --}}
	{{-- HTML::style('css/styles.css') --}}
	{{-- HTML::script('js/jquery.min.js') --}}
	{{-- HTML::script('js/bootstrap.min.js') --}}
    </head>
    <body>
	<div class="container" style="margin-top:10px;margin-left:10px;margin-right:10px">
		<div class="row" style="margin-bottom:15px">
			<span class="pull-left" style="margin-right:15px;">
				<a href="{{ URL::to('/') }}">
					<img src="/images/wkclogosmall.png" />
				</a>
			</span>
			<h2>{{ Lang::get('vf.app_title') }}</h2>
			@include('menu')
		</div>
		<div class="row">
			@yield('content')
		</div>
	</div>
    </body>
</html>
