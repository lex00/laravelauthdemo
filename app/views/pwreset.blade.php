@extends('layout')

@section('content')
{{ Form::open(array('url' => 'auth/pwreset')) }}
	{{ Form::hidden('token', $token) }}
	@if (Session::has('error'))
	<div class="row">
	<div class="col-md-4 form-group has-error">
		 <span class="help-block"><strong>{{ Lang::get(Session::get('reason')) }}</strong></span>
	</div>
	</div>
	@endif
	
	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
		<label for="email" class="control-label">{{ Lang::get('vf.signup_email') }}</label>
		<input type="text" name="email" id="email" class="form-control"
			placeholder="{{ Lang::get('vf.signup_email_default') }}"
			value="{{ Form::getValueAttribute('email') }}" />
		@if ( $errors->has('email') )
		    <span class="help-block">{{ $errors->first('email') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
		<label for="password" class="control-label">{{ Lang::get('vf.signup_password') }}</label>
		<input type="password" name="password" id="password" class="form-control"
			value="" />
		@if ( $errors->has('password') )
		    <span class="help-block">{{ $errors->first('password') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
		<label for="password_confirmation" class="control-label">{{ Lang::get('vf.pwreset_confirm') }}</label>
		<input type="password" name="password_confirmation" id="password_confirmation" class="form-control"
			value="" />
		@if ( $errors->has('password_confirmation') )
		    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 text-center">
		<input type="submit" 
		class="{{ Config::get('vf.formbuttoncss') }}" 
		value="{{ Lang::get('vf.pwreset_submit') }}" />
	</div>
	</div>

{{ Form::close() }}

@stop
