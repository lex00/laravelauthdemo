@extends('layout')

@section('content')
{{ Form::open(array('url' => 'auth/pwreminder')) }}
	@if (Session::has('error') || Session::has('success'))
	<div class="row">
		@if (Session::has('error'))
			<div class="col-md-4 form-group has-error">
			<span class="help-block"><strong>{{ Lang::get(Session::get('reason')) }}</strong></span>
		@elseif (Session::has('success'))
			<div class="col-md-4 form-group has-success">
			<span class="help-block"><strong>{{ Lang::get('vf.pwreminder_success') }}</strong></span>
		@endif
	</div>
	</div>
	@endif

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
		<label for="email" class="control-label">{{ Lang::get('vf.signup_email') }}</label>
		<input type="text" name="email" id="email" class="form-control"
			placeholder="{{ Lang::get('vf.signup_email_default') }}"
			value="{{ Form::getValueAttribute('email') }}" />
		@if ( $errors->has('email') )
		    <span class="help-block">{{ $errors->first('email') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 text-center">
		<input type="submit" 
		class="{{ Config::get('vf.formbuttoncss') }}" 
		value="{{ Lang::get('vf.pwreminder_submit') }}" />
	</div>
	</div>

{{ Form::close() }}

@stop
