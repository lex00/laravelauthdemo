@extends('layout')

@section('content')
<p class="lead">{{ Lang::get('vf.signup_thanks1', array('name' => Auth::user()->getFirstName()) ) }}</p>
<p class="lead">{{ Lang::get('vf.signup_thanks2') }}</p>

@stop
