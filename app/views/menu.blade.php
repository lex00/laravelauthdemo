<a href="{{ URL::to('/') }}" class="{{ Config::get('vf.menubuttoncss') }}">
	{{ Lang::get('vf.menu_records') }}
</a>

@if ( !Auth::check() )
	|
	<a href="{{ URL::to('auth/signup') }}" class="{{ Config::get('vf.menubuttoncss') }}">
		{{ Lang::get('vf.menu_signup') }}
	</a>
	|
	<a href="{{ URL::to('auth/signin') }}" class="{{ Config::get('vf.menubuttoncss') }}">
		{{ Lang::get('vf.menu_signin') }}
	</a>
@endif

@if ( Auth::check() )
	|
	<a href="{{ URL::to('auth/edit-profile') }}" class="{{ Config::get('vf.menubuttoncss') }}">
		{{ Lang::get('vf.menu_editprofile') }}
	</a>
	|
	<a href="#" class="{{ Config::get('vf.menubuttoncss') }}">
		{{ Lang::get('vf.menu_addrecord') }}
	</a>
	|
	<a href="{{ URL::to('auth/signout') }}" class="{{ Config::get('vf.menubuttoncss') }}">
		{{ Lang::get('vf.menu_signout') }}
	</a>
@endif
