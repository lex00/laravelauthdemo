@extends('layout')

@section('content')
<p class="lead">{{ Lang::get('vf.editprofile_header') }}</p>
{{ Form::open(array('url' => 'auth/edit-profile')) }}
	@if ( isset($success) )
	<div class="row">
	<div class="col-md-4 form-group has-success">
		<span class="help-block"><strong>{{ Lang::get('vf.editprofile_success') }}</strong></span>
	</div>
	</div>
	@endif

	<div class="row">
	<div class="col-md-4">
		<label>{{ Lang::get('vf.signup_name_first') }}:</label> {{ Auth::user()->getFirstName() }}
	</div>
	</div>

	<div class="row">
	<div class="col-md-4">
		<label>{{ Lang::get('vf.signup_name_last') }}:</label> {{ Auth::user()->getLastName() }}
	</div>
	</div>

	<div class="row">
	<div class="col-md-4">
		<label>{{ Lang::get('vf.signup_gender') }}:</label> {{ Auth::user()->getGenderLabel() }}
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('country') ? ' has-error' : '' }}">
		<label for="country" class="control-label">{{ Lang::get('vf.signup_country') }}</label>
		<select name="country" id="country">
			@foreach (Lang::get('locations.countries') as $countrycode => $countryname)
				<option 
				  value="{{ $countrycode }}"
				  {{ Form::getValueAttribute('country') == $countrycode ? " selected='selected'" : '' }}
				> {{ $countryname }} </option>
			@endforeach
		</select>
		@if ( $errors->has('country') )
		    <span class="help-block">{{ $errors->first('country') }}</span>
		@endif
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('url_website') ? ' has-error' : '' }}">
		<label for="url_website" class="control-label">{{ Lang::get('vf.signup_url_website') }}</label>
		<input type="text" name="url_website" id="url_website" class="form-control"
			placeholder="{{ Lang::get('vf.signup_url_website_default') }}"
			value="{{ Form::getValueAttribute('url_website') }}" />
		@if ( $errors->has('url_website') )
		    <span class="help-block">{{ $errors->first('url_website') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
		<label for="email" class="control-label">{{ Lang::get('vf.signup_email') }}</label>
		<input type="text" name="email" id="email" class="form-control"
			placeholder="{{ Lang::get('vf.signup_email_default') }}"
			value="{{ Form::getValueAttribute('email') }}" />
		@if ( $errors->has('email') )
		    <span class="help-block">{{ $errors->first('email') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
		<label for="password" class="control-label">{{ Lang::get('vf.signup_password') }}</label>
		<input type="password" name="password" id="password" class="form-control"
			value="" />
		@if ( $errors->has('password') )
		    <span class="help-block">{{ $errors->first('password') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
		<label for="password_confirmation" class="control-label">{{ Lang::get('vf.pwreset_confirm') }}</label>
		<input type="password" name="password_confirmation" id="password_confirmation" class="form-control"
			value="" />
		@if ( $errors->has('password_confirmation') )
		    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 text-center">
		<input type="submit" 
		class="{{ Config::get('vf.formbuttoncss') }}" 
		value="{{ Lang::get('vf.editprofile_submit') }}" />
	</div>
	</div>

{{ Form::close() }}

@stop
