@extends('layout')

@section('content')
<p class="lead">{{ Lang::get('vf.signin_header') }}</p>
{{ Form::open(array('url' => 'auth/signin')) }}
	@if ( isset($login_errors) )
	<div class="row">
	<div class="col-md-4 form-group has-error">
		 <span class="help-block"><strong>{{ Lang::get('vf.signin_invalid') }}</strong></span>
	</div>
	</div>
	@endif

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
		<label for="email" class="control-label">{{ Lang::get('vf.signup_email') }}</label>
		<input type="text" name="email" id="email" class="form-control"
			placeholder="{{ Lang::get('vf.signup_email_default') }}"
			value="{{ Form::getValueAttribute('email') }}" />
		@if ( $errors->has('email') )
		    <span class="help-block">{{ $errors->first('email') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group {{ $errors->has('password') ? ' has-error' : '' }}">
		<label for="password" class="control-label">{{ Lang::get('vf.signup_password') }}</label>
		<input type="password" name="password" id="password" class="form-control"
			value="" />
		@if ( $errors->has('password') )
		    <span class="help-block">{{ $errors->first('password') }}</span>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 form-group">
		<label for="rememberme">
			<input type="checkbox" name="rememberme" id="rememberme" value="y" 
			  {{ Form::getValueAttribute('rememberme') == 'y' ? ' checked' : '' }}
			/>
			{{ Lang::get('vf.signin_remember') }}
		</label>
	</div>
	</div>

	<div class="row">
	<div class="col-md-4 text-center">
		<input type="submit" 
			class="{{ Config::get('vf.formbuttoncss') }}" 
			value="{{ Lang::get('vf.signin_submit') }}" />
		<br />
		<br />
		<a href="{{ URL::to('auth/pwreminder') }}" class="btn btn-warning btn-xs">
			{{ Lang::get('vf.signin_lostpw') }}
		</a>
	</div>
	</div>

{{ Form::close() }}

@stop
