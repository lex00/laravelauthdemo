@extends('layout')

@section('content')
    @foreach($users as $user)
        <p><ul>
		<li>First Name = {{ $user->getFirstName() }}</li>
		<li>Last Name = {{ $user->getLastName() }}</li>
		<li>Email = {{ $user->getReminderEmail() }}</li>
		<li>Website = {{ $user->getWebsiteUrl() }}</li>
		<li>Active = {{ $user->getActive() }}</li>
	</ul></p>
    @endforeach

    {{App::environment();}}

@stop
