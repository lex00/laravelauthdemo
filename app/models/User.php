<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';



	/**
	 * The attributes excluded from mass assignment
	 *
	 * @var array
	 */
	protected $guarded = array('id', 'password');



	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');



	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}



	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Set the password for the user.
	 *
	 * @param string $password the password to set
	 */
	public function setAuthPassword($password)
	{
		$this->password = $password;
	}



	/**
	 * Get the e-mail address of the user
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Set the e-mail address of the user
	 *
	 * @param string $email the email to set
	 */
	public function setReminderEmail($email)
	{
		$this->email = $email;
	}



	/**
	 * Get the first name of the user
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->name_first;
	}

	/**
	 * Set the first name of the user
	 *
	 * @param string $name_first the first name to set
	 */
	public function setFirstName($name_first)
	{
		$this->name_first = $name_first;
	}



	/**
	 * Get the active status of the user
	 *
	 * @return boolean
	 */
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * Set the active status of the user
	 *
	 * @param boolean
	 */
	public function setActive($active)
	{
		$this->active = $active;
	}
	


	/**
	 * Get the last name of the user
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->name_last;
	}

	/**
	 * Set the last name of the user
	 *
	 * @param string $name_last the last name to set
	 */
	public function setLastName($name_last)
	{
		$this->name_last = $name_last;
	}



	/**
	 * Get the gender of the user m|f
	 *
	 * @return string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Set the gender of the user m|f
	 *
	 * @return string
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * Get the gender label
	 *
	 * @return string
	 */
	public function getGenderLabel()
	{
		if( $this->gender == 'm' )
		{
			return Lang::get('vf.signup_gender_m');
		} else {
			return Lang::get('vf.signup_gender_f');
		}
	}



	/**
	 * Get the country
	 *
	 * @return string
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * Set the country of the user
	 *
	 * @return string
	 */
	public function setCountry($country)
	{
		$this->country = $country;
	}



	/**
	 * Get the website url
	 *
	 * @return string
	 */
	public function getWebsiteUrl()
	{
		return $this->url_website;
	}

	/**
	 * Set the website url
	 *
	 * @param string $url_website the url to set
	 */
	public function setWebsiteUrl($url_website)
	{
		$this->url_website = $url_website;
	}


}
