<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		if (App::environment() != 'testing')
		{
			//production
			$this->call('UserTableSeeder');
			$this->command->info('User table seeded!');
		} else {
			//testing
			$this->call('TestTableSeeder');
		}
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('email' => 'service@worldkettlebellclub.com',
			'password' => $password = Hash::make('wkcadmin99'),
			'name_first' => 'Valery',
			'name_last' => 'Fedorenko',	
			'url_website' => 'http://worldkettlebellclub.com',	
			'country' => 'US',
			'gender' => 'm',
			'active' => '1'
			));
    }

}



class TestTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('email' => Config::get('app.tests.email'),
			'password' => $password = Hash::make(Config::get('app.tests.password')),
			'name_first' => Config::get('app.tests.name_first'),
			'name_last' => Config::get('app.tests.name_last'),	
			'url_website' => Config::get('app.tests.url_website'),	
			'country' => Config::get('app.tests.country'),
			'gender' => Config::get('app.tests.gender'),
			'active' => Config::get('app.tests.active'),
			));
    }

}
